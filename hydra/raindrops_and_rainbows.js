// Playing Shakles by Mentz.
// https://freemusicarchive.org/music/Mentz/Re-Locate_EP/04_mentz_shackles

a.show();
a.setSmooth(0.9999);
s0.initCam();

src(s0).out(o0);

osc(()=>a.fft[0]*10, ()=>a.fft[3]*0.8, 2)
  .pixelate()
  .kaleid(({time})=>Math.sin(time)*0.5)
  .add(
    shape(100, 0.01, 1)
      .invert(({time})=>Math.sin(time)*2*a.fft[1])
      .posterize(()=>3*a.fft[2])
  )
  .modulatePixelate(o0)
  .out(o1);

render(o1);
